const getCreateTaskDiv = document.getElementById("createTask");
const getNewTasks = document.getElementById("newTasks")
const getInputValue = document.getElementById("taskInputValue")
const deleteButtons = document.getElementsByClassName("deleteTask")
const getUlDone = document.getElementById("taskDone")
const eraseAllToDo = document.getElementById("eraseAllToDo")
const eraseAllDone = document.getElementById("eraseAllDone")
const numOfTasks = document.getElementById("numOfTasks")
const login = document.getElementById("loginBtn")
const mainContainer = document.getElementById("overlay")
const btnSignIn = document.getElementById("signIn")
const getLogin = document.getElementById("userName")
const getPassword = document.getElementById("password")
const loginContainer = document.getElementById("hideContainer")
const btnClose = document.getElementById("close")
let counter = 0;

const database = {
    login: "gustavo@cooper.com",
    password: "123teste"
}

getCreateTaskDiv.addEventListener("keypress", (evt) => {
    if (evt.key === "Enter") {
        const div = document.createElement("div");
        div.classList.add("container");
        div.classList.add("block");
        getNewTasks.appendChild(div);
        const createDivCheckbox = document.createElement("div");
        createDivCheckbox.classList.add("checkbox")
        div.appendChild(createDivCheckbox);
        const createLi = document.createElement("li");
        createLi.classList.add("taskToDo");
        createLi.dataset.todoId = `todo${counter}`
        createLi.innerText = getInputValue.value
        div.appendChild(createLi);
        const createBtn = document.createElement("button");
        createBtn.onclick = (evt) => {
            evt.target.parentNode.parentNode.removeChild(evt.target.parentNode);
        }
        createBtn.classList.add("deleteTask");
        createBtn.innerText = "delete"
        createBtn.dataset.taskId = `task${counter}`
        div.appendChild(createBtn);
        counter++;
        getInputValue.value = ""
        $(document).ready(function() {
            $( function() {
                $( ".block" ).draggable();
              } );
        })
        
    }   
})

const removeAllTasks = () => {
    const newTasksChildren = document.getElementById("newTasks");
    let counter = newTasksChildren.childElementCount
    for (let i = 0; i < counter; i++) {
        eraseAllToDo.previousElementSibling.firstElementChild.remove()
    }
}

eraseAllToDo.addEventListener("click", (evt) => {
    removeAllTasks();
    getNumOfTasks();
})


getNewTasks.addEventListener("click", (evt) => {
    let target = evt.target; 
    let taskDone;
    if (target.className === "checkbox") {
        target.classList.add("checkDone");
        target.classList.remove("checkbox");
    }
    if (target.className === "checkDone") {
        taskDone = target.parentElement.parentElement.removeChild(target.parentElement)
    }
    getUlDone.appendChild(taskDone);
    getNumOfTasks();
})

getUlDone.addEventListener("click", (evt) => {
    let target = evt.target;
    let taskUndone;
    if (target.className === "checkDone") {
        target.classList.remove("checkDone");
        target.classList.add("checkbox");
    }
    if (target.className === "checkbox") {
        taskUndone = target.parentElement.parentElement.removeChild(target.parentElement)
    }
    getNewTasks.appendChild(taskUndone);
    getNumOfTasks();
})

const removeAllDoneTasks = () => {
    let counter = getUlDone.childElementCount
    for (let i = 0; i < counter; i++) {
        eraseAllDone.previousElementSibling.firstElementChild.remove()
    }
}

eraseAllDone.addEventListener("click", (evt) => {
    removeAllDoneTasks();
    getNumOfTasks();
})

const getNumOfTasks = () => {
    numOfTasks.innerText = getUlDone.childElementCount; 
}

login.addEventListener("click", () => {
    mainContainer.classList.add("overlay");
    loginContainer.classList.remove("hidden")
})

btnSignIn.addEventListener("click", (evt) => {
    if (getLogin.value === database.login &&
        getPassword.value === database.password) {
        console.log("login concluido");
        mainContainer.classList.remove("overlay")
        loginContainer.classList.add("hidden");  
    }
    else {
        console.log("voce digitou o login errado")
    }
})

btnClose.addEventListener("click", () => {
    mainContainer.classList.remove("overlay");
    loginContainer.classList.add("hidden")
})

$(init);
function init() {
    console.log("working")
    $(".done").droppable(

    {
        accept: ".block",
        position: "unset",
        drop: function(evt, ui) {
            console.log("event was dropped")
            console.log($(this))
            let droppedItem = $(ui.draggable);
            $(droppedItem).appendTo($("#taskDone"));
        }
    })
}

